'use strict';

var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();
var reviewsHtml = "<li>読み込み中</li>"

// 商品名取得部分
var itemId = 0;
var itemName = "デフォルトアイテム"
var params = {
    TableName: 'Items',
    Key:{//取得したい項目をプライマリキー(及びソートキー)によって１つ指定
         id: 1
    }
};

// 商品名を取得するためのパラメータ
var params = {
    TableName: 'Items',
    Key:{//取得したい項目をプライマリキー(及びソートキー)によって１つ指定
         id: 1
    }
};

// レビューを取得するためのパラメータ
var params2 = {
    TableName : "Reviews",
    IndexName: "items_id-index",
    KeyConditionExpression: "#items_id = :items_id",
    ExpressionAttributeNames:{
        "#items_id": "items_id"
    },
    ExpressionAttributeValues: {
        ":items_id": itemId
    }
};

// 商品名を取得
function dbAccess(itemId) {
    params = {
        TableName: 'Items',
        Key:{//取得したい項目をプライマリキー(及びソートキー)によって１つ指定
             id: itemId
        }
    };

    docClient.get(params, function(err, data){
        if(err){
            console.log(err);
            itemName = err
        }else{
            itemName = data.Item.name;
        }
    });
}

// レビューを取得
function dbAccess2(itemId) {
    var params2 = {
        TableName : "Reviews",
        IndexName: "items_id-index",
        KeyConditionExpression: "#items_id = :items_id",
        ExpressionAttributeNames:{
            "#items_id": "items_id"
        },
        ExpressionAttributeValues: {
            ":items_id": itemId
        }
    };

    docClient.query(params2, function(err, data) {
        reviewsHtml = "<table><tr><td><b>Title</b></td><td><b>Message</b></td></tr>";
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            reviewsHtml = err;
        } else {
            data.Items.forEach(function(item) {
                reviewsHtml = reviewsHtml + "<tr><td>" + item.title + "</td><td>" + item.message + "</td></tr>";
            });
        }
        reviewsHtml = reviewsHtml + "</table>";
    });
}

// ハンドラ
module.exports.hello = (event, context, cb) => {
    itemId = event['itemId']
    itemId = Number(itemId)
    dbAccess(itemId)
    dbAccess2(itemId)
    setTimeout(callback, 600)
    function callback() {
        var renderedPage = renderFullPage();
        cb(null, renderedPage );
    }
};

// HTML
function renderFullPage() {
  return `<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>ここは${itemName}のページだよ</h1>
        <br>
        ${reviewsHtml}
        <form method="post" action="https://e1v66r86dl.execute-api.us-east-1.amazonaws.com/dev" accept-charset="UTF-8">
            <input type="hidden" name="items_id" value="${itemId}">
            Title<input type="text" name="title" value="タイトル"><br>
            Review<input type="text" name="message"><input type="submit" value="送信">
        </form>
        <br>
        <a href="https://petoo.shino-naoki-rensyu.com/itemList">商品一覧ページに戻る</a><br>
        <a href="https://petoo.shino-naoki-rensyu.com/top">TOPページに戻る</a>
    </body>`
    ;
}