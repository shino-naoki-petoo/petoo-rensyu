'use strict';

// Your first function handler
module.exports.hello = (event, context, cb) => {
    var renderedPage = renderFullPage( 'このメッセージは動的に出力しています');
  cb(null, renderedPage );
};

// You can add more handlers here, and reference them in serverless.yml

function renderFullPage(renderedContent) {
  return `<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>ここは商品一覧ページだよ</h1>
        <a href="https://petoo.shino-naoki-rensyu.com/item/1">究極の猫砂</a><br>
        <a href="https://petoo.shino-naoki-rensyu.com/item/2">至極のリード</a><br>
        <a href="https://petoo.shino-naoki-rensyu.com/item/3">Perfect Dog Food</a><br>
        <br>
        <a href="https://petoo.shino-naoki-rensyu.com/top">TOPページに戻る</a>
    </body>
</html>`;
}