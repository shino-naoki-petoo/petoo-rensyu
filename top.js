'use strict';

// Your first function handler
module.exports.hello = (event, context, cb) => {
    var renderedPage = renderFullPage( 'このメッセージは動的に作成しています');
  cb(null, renderedPage );
};

// You can add more handlers here, and reference them in serverless.yml

function renderFullPage(renderedContent) {
  return `<!DOCTYPE html>
<html>
    <style type="text/css">
    p {color:blue; line-height:1.5;}
    </style>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>ここはTOPページだよ</h1>
        <p>Pタグ部分</p>
        <div id="container">${renderedContent}</div>
        <a href="https://petoo.shino-naoki-rensyu.com/itemList">商品一覧ページへ</a>
    </body>
</html>`;
}